# Emotion Tweets Analyzer

This project saves tweets from a search and determines the emotion and keywords. The result is save to the file *logs.txt*

## Implementation

You need to create a file *config.ini* with parameters :

`[DEFAULT]`
`tweeter_public_key = `
`tweeter_private_key = `
`tweeter_private_token = `
`tweeter_public_token = `
`openai_api_key =`
`tweeter_last_id = `

## Execution of script

Using Cron to execute task every 12h from Monday through Friday (in our exemple TPMP is not broadcast on weekends)

` 0 */12 * * 1-5 root python main.py `
