import openai

class OPenAIAnalyzer:
	
	def __init__(self,apiKey):
		openai.api_key = apiKey
		
	def getEmotion(self,content):
		emotion = openai.Completion.create(
	  		engine="text-davinci-001",
	  		prompt= "Decide whether a Tweet's sentiment is positive, neutral, or negative. Tweet: '"'{}'"' Sentiment:".format(content),
	  		temperature=0,
	  		max_tokens=60,
	  		top_p=1.0,
	  		frequency_penalty=0.5,
	  		presence_penalty=0.0)        
		return emotion.choices[0].text
	
	def getKeyWord(self,content):
		keyWord = openai.Completion.create(
			engine="text-davinci-001",
			prompt= "Extract keywords from this text:  '"'{}'"'. Keyword : ".format(content),
			temperature=0,
			max_tokens=60,
			top_p=1.0,
			frequency_penalty=0.5,
			presence_penalty=0.0) 
		return keyWord.choices[0].text
	
	