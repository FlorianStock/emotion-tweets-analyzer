import tweepy
from datetime import datetime
from fileManage import config, saveConfig
from openAI import OPenAIAnalyzer

class TweeterClient:
    def __init__(self):
        
        auth = tweepy.OAuthHandler(
            config.get('DEFAULT', 'TWEETER_PUBLIC_KEY'), 
            config.get('DEFAULT', 'TWEETER_PRIVATE_KEY'))
        
        auth.set_access_token(
            config.get('DEFAULT', 'TWEETER_PRIVATE_TOKEN'), 
            config.get('DEFAULT', 'TWEETER_PUBLIC_TOKEN'))
        
        self.api = tweepy.API(auth)
        
        try:
            self.api.verify_credentials()     
        except:
            print("Error during authentication")
            
    def search(self,keyword):
        self.tweets = self.api.search_tweets(
            q =keyword,lang="fr",
            result_type="recent", 
            count = 15, 
            since_id = config.get("DEFAULT","TWEETER_LAST_ID") ,        
            tweet_mode="extended")
      
    def analyzeWithOpenAI(self):      
        analyzer = OPenAIAnalyzer(config.get('DEFAULT', 'OPENAI_API_KEY'))
        records = []
        for tweet in self.tweets:           
            record = {
		        "created_at" : datetime.now().isoformat(),
		        "id" : tweet._json["id"],
		        "emotion" : analyzer.getEmotion(tweet._json["full_text"]),
 		        "keyWord" : analyzer.getKeyWord(tweet._json["full_text"])
            }
            records.append(record)
            
        if len(records)>0:    
            config.set("DEFAULT","TWEETER_LAST_ID",str(records[len(records)-1]["id"]))
        saveConfig()
        
        return records
	     
    
        
        
        
        