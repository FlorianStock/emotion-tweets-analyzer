
import json
import os
import configparser
config = configparser.ConfigParser()
config.read('config.ini')

def saveConfig():
    with open('config.ini', 'w') as configfile:
        config.write(configfile)
    
        
def saveLogsList(results):  
  with open('logs.txt', 'a',encoding="utf-8") as f:
    for result in results:
      jsonString = json.dumps(result)   
      f.write(jsonString)
      f.write('\n')
    f.close()

