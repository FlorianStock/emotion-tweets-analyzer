from fileManage import saveLogsList
from tweeterClient import TweeterClient

SEARCH = "tpmp"

client = TweeterClient()
client.search(SEARCH)

results = client.analyzeWithOpenAI()

saveLogsList(results)